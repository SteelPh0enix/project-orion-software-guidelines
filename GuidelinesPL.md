# Wytyczne nisko- i wysokopoziomowego oprogramowania w językach C i C++ Projektu Orion

## Wstęp

W tym repozytorium znajdują się wytyczne dotyczące tworzenia kodu nisko- i wysokopoziomowego do łazika tworzonego w Projekcie Orion, oraz boilerplate'y i przykłady kodu.

## Ogólne wymagania dotyczące wiedzy i umiejętności osoby uczestniczącej w projekcie

Wymagania te aplikują się do wszystkich wymagań zawartych w innych dokumentach.

### Wiedza podstawowa

* **Znajomość technicznego angielskiego** - poziom podstawowy, pozwalający czytać z pomocą Google Translate.
* **Umiejętność używania Google** - pierwsze źródło informacji na temat każdego napotkanego problemu
* **Komunikatywność** - umiejętność porozumiewania się z członkami zespołu
* **Umiejętność pisania podstawowej dokumentacji** - opisywanie swojej pracy w języku polskim/angielskim
* **Umiejętność korzystania z dokumentacji i referencji**
* **Chęć do pracy** - *obviously*

### Wiedza preferowana

* **Dobra znajomość technicznego angielskiego** - płynne czytanie oraz rozmowa
* **Umiejętność znajdowania, czytania i rozumienia datasheetów i manuali** - jest to podstawowe źródło wiedzy na temat układów, wlicza się w to kryterium również znajomość elektroniki/cyfrówki pozwalająca zrozumieć działanie układu
* **Doświadczenie w pracy z mikrokontrolerami** - najlepiej projekty lub kontrybucje do nich
* **Doświadczenie w pracy z systemami kontroli wersji** - głównie Git (Github/Gitlab). Umiejętność zrobienia commita i pushnięcia go do repo, oraz pracy na branchach.
* **Umiejętność pisania dokumentacji** - opisywanie swojej pracy w języku angielskim

Poszczególne wytyczne dla konkretnych zagadnień można znaleźć poniżej:

* [Wytyczne dotyczące tworzenia kodu niskopoziomowego (Arduino/AVR/STM32)](GuidelinesPL/LowLevelGuidelines.md)
* [Wytyczne dotyczące tworzenia kodu wysokopoziomowego w języku C++](GuidelinesPL/CPPGuidelines.md)
* [Materiały oraz linki pomocne przy pracy z projektem](GuidelinesPL/Resources.md)
