#!/bin/python3

# Remember to copy `serial_tester.py` to the test directory too, since it's dependency
from serial_tester import SerialTester

if __name__ == '__main__':
    # You can get port name either using `ls /dev/tty*` under Linux (you have to pass absolute path to device), or device manager under Windows (you can pass the name)
    tester = SerialTester('COM4')

    tester.send(b"prepare everything", measure_time=False)
    for i in range(0, 100):
        tester.send(b"request number " + str.encode(str(i)))

    tester.close()
    print("\n\n========== RESULTS ==========")
    print("Responses count: {}".format(tester.responses_count))
    print("Average request time: {0:.3f}ms".format(
        tester.average_request_time()))
    print("Average request frequency: {0:.3f}Hz".format(
        tester.average_request_frequency()))
