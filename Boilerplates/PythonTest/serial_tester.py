from serial import Serial, SerialException
from timeit import default_timer as timer


class SerialTester():
    def __init__(self, port_name, baud_rate=250000, timeout=5):
        # object config
        self.port_name = port_name
        self.baud_rate = baud_rate
        self.timeout = timeout

        # time measuring
        self.responses_time_sum = 0
        self.responses_count = 0

        self.connection = Serial(
            port=self.port_name, baudrate=self.baud_rate, timeout=self.timeout)

    def send(self, data, measure_time=True, add_newline=True):
        if add_newline:
            data += b'\n'

        print("Sending: " + str(data) + '\n' if add_newline else '')

        start = timer()
        self.connection.write(data)
        response = self.connection.readline()
        end = timer()

        print("Response: " + str(response))

        response_time = (end - start) * 1000
        timeout = False

        if response_time >= self.timeout * 1000:
            print("Timeout has been reached. Device did not responded.")
            timeout = True
        else:
            print("Response time: " + str(response_time) + "ms")
            if measure_time:
                self.responses_time_sum += response_time
                self.responses_count += 1

        return timeout

    def average_request_time(self):
        return self.responses_time_sum / self.responses_count

    def average_request_frequency(self):
        return 1 / (self.average_request_time() / 1000)

    def close(self):
        self.connection.close()
