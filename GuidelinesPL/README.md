# Wytyczne nisko- i wysokopoziomowego oprogramowania w językach C i C++ Projektu Orion

* [Wytyczne dotyczące tworzenia kodu niskopoziomowego (Arduino/AVR/STM32)](LowLevelGuidelines.md)
* [Wytyczne dotyczące tworzenia kodu wysokopoziomowego w języku C++](CPPGuidelines.md)
* [Materiały oraz linki pomocne przy pracy z projektem](Resources.md)
