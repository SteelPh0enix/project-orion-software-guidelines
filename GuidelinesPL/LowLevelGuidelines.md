# Wytyczne dotyczące tworzenia kodu niskopoziomowego (Arduino/AVR/STM32)

## Wstęp

W tym dokumencie są opisane wytyczne dotyczące desingu kodu niskopoziomowego jaki powinien zostać zastosowany w przypadku software'u Oriona, oraz wykorzystywanych narzędzi.

Bazą zaleceń dotyczących języka C++ są zalecenia ogólne dotyczące kodu wysokopoziomowego, które można znaleźć [tutaj](CPPGuidelines.md). Poniższe zalecenia mają jednak wyższy priorytet, więc jeśli któreś jest w konflikcie, do niskopoziomowego kodu należy stosować te z tego pliku.

Przydatne linki (również te wykorzystane tutaj) można znaleźć w [materiałach](Resources.md#Arduino)

### Wiedza potrzebna do prac na danym poziomie

Zakładając spełnienie wymogów bazowych do pracy w projekcie (patrz: [ogólne wytyczne](../GuidelinesPL.md)), można wyróżnić następujące poziomy wiedzy.

Ze względu na charakter opisywanych platform, do stanu wiedzy języków programowania nie zalicza się znajomości bibliotek standardowych, chyba, że jest wyraźnie zaznaczone inaczej.
Do każdego kolejnego poziomu wiedzy wlicza się stan wszystkich poprzednich.

#### Podstawowy

* **Pobieżna znajomość języka C** - składnia, podstawowe elementy języka
* **Pobieżna znajomość języka C++** - składnia, podstawowe elementy języka, podstawy obiektowości (umiejętność wykorzystania obiektów w praktyce)
* **Pobieżna znajomość języka [Python](https://www.python.org)** - umiejętność pisania prostych skryptów z wykorzystaniem gotowych bibliotek
* **Pobieżna znajomość praktycznej elektroniki** - prawo Ohma, prawa Kirchoffa, liczenie oraz szacowanie rezystancji/prądów/napięć
* **Pobieżna znajomość architektury mikrokontrolera** - z czego się składa, jakie ma możliwości
* **Podstawowa umiejętność obsługi urządzeń pomiarowych** - multimetr, pomiar prądu/napięcia/rezystancji, diagnostyka prostych układów

#### Średniozaawansowany

* **Średnia znajomość języka C** - biblioteka standardowa, struktury, makra
* **Średnia znajomość języka C++** - obiektowość, podstawy szablonów (przekazywanie typów), przeładowywanie operatorów, rule of five, podstawowe wzorce obiektowe (strategia, iterator, metoda szablonowa, obserwator, stan)
* **Średnia znajomość języka [Python](https://www.python.org)** - tworzenie własnych skryptów na podstawie dokumentacji, z wykorzystaniem dowolnych bibliotek
* **Średnia znajomość praktycznej elektroniki** - tworzenie i analiza prostych układów elektronicznych
* **Średnia znajomość architektury mikrokontrolera** - umiejętność obsługi przerwań i timerów oraz API w języku C
* **Średnia umiejętność obsługi urządzeń pomiarowych** - oscyloskop
* **Umiejętność obsługi lutownicy**

#### Zaawansowany

* **Dobra znajomość języka C** - projekty lub znaczna ilość doświadczenia
* **Dobra znajomość języka C++** - projekty lub znaczna ilość doświadczenia
* **Dobra znajomość architektury mikrokontrolera** - w szczególności API w języku C
* **Dobra umiejętność posługiwaniem się urządzeniami pomiarowymi** - samodzielne tworzenie nietrywialnych układów pomiarowych

### Arduino

Głównym frameworkiem za pomocą którego tworzymy niskopoziomowy software jest Arduino.

Zalety:

* Łatwa w użyciu biblioteka standardowa
* Łagodna krzywa uczenia się
* Duża ilość zewnętrznych bibliotek do gotowych modułów
* Szerokie wsparcie wśród community
* Wieloplatformowość
* Pozwala na szybkie prototypowanie

Wady:

* Nie jest tak szybki i optymalny jak pisanie w czystym C
* Brak jednolitego standardu bibliotek (co czasami powoduje mus pisania wrapperów)

Podstawowym źródłem wiedzy na temat standardowej biblioteki Arduino są [referencje](https://www.arduino.cc/reference/en/#page-title)

Wykorzystywane przez nas w projekcie płytki oparte na Arduino *(stan na dzień 30.09.2018r.)*

* Arduino Nano *(ATmega328 @ 16MHz/5V)*
* Arduino Mega 2560 *(ATMega2560 @ 16MHz/5V)*
* Arduino Due *(AT91SAM3X8E @ 84MHz/3.3V)*

#### IDE

Preferowane w projekcie IDE to [PlatformIO](https://docs.platformio.org/en/latest/)

Zalety:

* Świetne wsparcie dla wielu frameworków i platform
* [Bogata dokumentacja](https://docs.platformio.org/en/latest/)
* [Duże możliwości konfiguracji projektu](https://docs.platformio.org/en/latest/projectconf.html)
* Wieloplatformowe
* Współpracuje z różnymi edytorami (preferowany jest [Visual Studio Code](https://code.visualstudio.com))
* Możliwość działania z linii poleceń (co pozwala na przykład na skrypty umożliwiające aktualizacje softu OTA)

Wady:

* Feature'y takie jak unit testing i zdalny debugging dostępne tylko z płatną subskrybcją (zostanie to rozwiązane poprzez stworzenie biblioteki do unit testów)

Arduino IDE nie jest preferowane, ze względu na to że w porównaniu jest bardzo biedne w feature'y i nie ma żadnych możliwości konfiguracji projektu, co wyklucza stosowanie w nim poniższych wytycznych. Wszelkie boilerplate'y i biblioteki tworzone do wykorzystania w projekcie będą w zamyśle wspierać głównie PlatformIO.

### Wytyczne ogólne dotyczące języków C i C++

#### 1. Kompilacja, narzędzia i standardy języka

##### 1.1 Standard języka

* Preferowanym standardem języka C jest **C99**

  *Uzasadnienie: C11 i C18 nie wprowadziło żadnych znaczących dla nas zmian. C99 jest w zupełności wystarczający do naszych potrzeb.*

* Minimalnym standardem języka C jest **ANSI C (C89)**

  *Uzasadnienie: Wspierany przez dosłownie każdy kompilator jakiego moglibyśmy użyć.*

* Preferowanym standardem języka C++ jest **C++14**

   *Uzasadnienie: C++14 posiada dobre wsparcie kompilatorów na ten moment, znacznie lepsze wsparcie dla constexpr i dedukcję typów niż C++11 oraz generyczne lambdy. C++17 jeszcze nie jest na tyle popularny i wspierany żeby można było go zastosować w projekcie jako stabilne rozwiązanie. Ponad to, C++14 jest sprawdzonym rozwiązaniem i dobrze współpracuje z PlatformIO.*

* Minimalnym standardem języka C++ jest **C++11**

  *Uzasadnienie: Wspierany przez wszystkie nowoczesne kompilatory. Nie planujemy używać takich, które nie mają wsparcia dla tego standardu.*

Informacje na temat szczegółowych możliwości standardów C++11 oraz C++14 można znaleźć [tutaj](https://en.cppreference.com/w/cpp/compiler_support)

##### 1.2 Zalecane flagi kompilatora

* Dla *avr-gcc*: `-Wall -Wextra -Wpedantic -std=c99`
* Dla *avr-g++*: `-Wall -Wextra -Wpedantic -std=c++14`

Włączenie pedantycznych ostrzeżeń kompilatora pozwoli na uniknięcie wielu cichych i trudnych do znalezienia błędów.

W PlatformIO flagi można ustawić w pliku `platformio.ini`, dla środowiska budowania, z użyciem parametru `build_flags`. Patrz: [dokumentacja PlatformIO](https://docs.platformio.org/en/latest/projectconf/section_env_build.html)

##### 1.3 Zalecenia dotyczące ostrzeżeń kompilatora

Jeśli wykorzystujemy zalecenia dotyczące flag, należy kierować się polityką `0 errors, 0 warnings`. Żeby kod spełniał zalecenia projektowe, musi on się kompilować bez błędów ani ostrzeżeń.

Kompilator pomaga pilnować standardu języka i poprawności kodu sprawdzając go i wyświetlając ostrzeżenia które wskazują na potencjalne błędy w kodzie. Należy bezwzględnie brać to pod uwagę.

***Uwaga**: nie dotyczy to ostrzeżeń które nie dotyczą bezpośrednio kodu pisanego przez członka projektu, tylko (przykładowo) biblioteki standardowej Arduino lub zewnętrznych bibliotek (o ile działają i nie stanowią potencjalnego zagrożenia dla stabilności oraz poprawności reszty kodu).*

##### 1.4 Dodatkowe narzędzia do analizy kodu

Do formatowania i statycznej analizy kodu (m.in. linting) zaleca się użyć narzędzi Clang, w celu utrzymania standardu formatowania kodu.

**Zalecanym stylem kodu jest styl Google.**

##### 1.5 Używanie zewnętrznych bibliotek

Zalecane jest wykorzystywanie tylko bibliotek znajdujących się w repozytorium PlatformIO, poprzez deklarowanie ich w pliku konfiguracyjnym projektu (`platformio.ini`).

W tym celu, należy w sekcji dotyczącej środowiska kompilacji dołączyć daną bibliotekę pod kluczem `lib_deps`. W przypadku dołączania kilku bibliotek, należy je oddzielać `,`. Po więcej informacji, patrz: sekcja `Installation` danej biblioteki w menadżerze bibliotek PlatformIO.

W przypadku wykorzystywania bibliotek które nie znajdują się w repozytorium PIO, jeśli ich licencja na to pozwala należy je dołączyć bezpośrednio do projektu, z uwzględnieniem zasad licencyjnych danej biblioteki.

#### 2. Jakość kodu oraz zalecane praktyki

##### 2.1 Unikać używania biblioteki standardowej języka C++

Biblioteka standardowa języka C++ nie była tworzona pod zastosowania niskopoziomowe. Należy unikać jej wykorzystywania.

Toolchain dostarczany z PlatformIO dla platformy AVR nie zawiera (z tego co osobiście zauważyłem) nagłówków biblioteki standardowej C++, więc odgórnie uniemożliwia jej wykorzystanie.

Alternatywą dla biblioteki standardowej C++ jest biblioteka standardowa C, której używanie jest dozwolone i zalecane.

Podpunkt ten rzecz jasna nie dotyczy feature'ów samego języka (lambdy, constexpr, szablony itp.)

##### 2.2 Unikać dynamicznej alokacji pamięci

Dynamiczne alokacje pamięci na mikrokontrolerach mogą wywoływać trudne do zdiagnozowania i naprawienia błędy (na przykład nagłe zawieszanie się mikrokontrolera) w momencie w którym ilość wolnej pamięci stanie się krytycznie niska. Należy za wszelką cenę unikać dynamicznej alokacji, zastępując ją statycznymi buforami.

Dodatkowo, w przypadku kiedy nie da się w żaden sposób uniknąć dynamicznych alokacji, używanie `new` i `malloc` oraz podobnych funkcji alokujących pamięć, bez `RAII`, jest surowo zabronione.

Dodatkowo, warto wprowadzić sprawdzanie przepełnienia wyżej wymienionych buforów.

**Przykłady:**

Źle

```cpp
int* buffer = new int[10];
```

Dobrze

```cpp
int buffer[10];
```

##### 2.3 Unikać statycznej inicjalizacji obiektów i stanu mikrokontrolera

Nie wolno inicjalizować żadnych funkcji mikrokontrolera (przykładowo pinMode, Serial.begin) w przestrzeni globalnej (poza funkcjami). Należy to robić w funkcji `setup` lub przed pętlą główną funkcji `main`.
Jeśli tworzymy obiekt który wykorzystuje funkcje mikrokontrolera które muszą być uprzednio zainicjalizowane, nie należy ich inicjalizować w konstruktorze (najlepiej jest pozostawić domyślny konstruktor i zaimplementować funkcję która zainicjalizuje wszystkie wykorzystywane elementy mikrokontrolera)

**Przykłady:**

<details>
<summary>Źle</summary>

```cpp
#include <Arduino.h>

class LED {
public:

  // Konstruktor inicjalizujący - domyślny nie będzie wygenerowany, co powoduje że nie można stworzyć obiektu bez ustawiania pinu od razu.
  LED(int pin)
    : m_pin{pin}
  {
    // Ustawianie stanów pinów w konstruktorze, czyli przy inicjalizacji obiektu zmieniamy stan wewnętrzny mikrokontrolera co nie jest bezpieczne w każdym przypadku
    pinMode(m_pin, OUTPUT);
    digitalWrite(m_pin, m_state);
  }

  void switch() {
    m_state = !m_state;
    digitalWrite(m_pin, m_state);
  }

private:
  int m_pin{0};
  bool m_state{false};
}

// Stworzenie globalnej instancji obiektu i jednoczesna inicjalizacja, w tym przypadku może po prostu nie zadziałać
LED led(LED_BUILTIN);
// LED led2; // brak domyślnego konstruktora, nie skompiluje się

void setup() {

}

void loop() {
  led.switch();
  delay(400);
}
```
</details>

<details>
<summary>Dobrze</summary>

```cpp
#include <Arduino.h>

class LED {
public:
  // Zapewnienie wygenerowania domyślnego konstruktora
  LED() = default;

  // Konstruktor ustawia jedynie pin na którym dioda jest podłączona - ma wpływ jedynie na wewnętrzny stan klasy, co jest bezpieczne.
  LED(int pin) {
    setPin(pin);
  }

  // Setter pozwalający ustawić pin na którym jest dioda w przypadku inicjalizacji domyślnym konstruktorem
  void setPin(int pin) {
    m_pin = pin;
  }

  // Osobna funkcja do inicjalizacji funkcji mikroprocesora
  void initialize() {
    pinMode(m_pin, OUTPUT);
    digitalWrite(m_pin, m_state);
  }

  void switch() {
    m_state = !m_state;
    digitalWrite(m_pin, m_state);
  }

private:
  int m_pin{0};
  bool m_state{false};
}

// Ten konstruktor tylko ustawia wewnętrzny stan obiektu, bezpieczne
LED led1(LED_BUILTIN);
LED led2; // Domyślny konstruktor istnieje

void setup() {
  // Inicjalizacja w setup jest bezpieczna i preferowana
  led.initialize();

  led2.setPin(10);
  led2.initialize();
}

void loop() {
  led.switch();
  led2.switch();
  delay(400);
}
```
</details>

##### 2.4 Pisać obiektowo (KISS/SOLID?)

Każdy element tworzonej części projektu powinien być - w miarę możliwości - przedstawiony w postaci obiektowej, co pozwoli na łatwe testowanie kodu i powtórne go używanie.
Należy jednak unikać bardziej skomplikowanych hierarchii obiektowych (dziedziczenia), jeśli to możliwe.

##### 2.5 Testować kod

Każdy obiekt powinien być odpowiednio przetestowany z wykorzystaniem dostępnych środków. Testy powinny być możliwe do uruchomienia w dowolnym momencie po ich stworzeniu.
Testowanie powinno odbywać się z użyciem magistrali szeregowej (serial) oraz serial monitora lub odpowiednich skryptów napisanych w wybranym przez siebie języku (sugerowany jest [Python](https://www.python.org) z biblioteką pySerial)

Przykładowo, można stworzyć osobny katalog z testami w którym będą trzymane odpowiedniki `main.cpp` i skrypty Pythonowe. Kod C/C++ można w dowolnym momencie podmienić i uruchomić, testując czy istniejąca architektura działa poprawnie.

Przykładowy skrypt Pythonowy razem z pomocniczą biblioteką można znaleźć [tutaj](../Boilerplates/PythonTest/test_template.py)

##### 2.6 Trzymać definicje które mogą zależeć od zewnętrznych czynników (numery pinów, ustawienia magistrali) w separacji od kodu

Dzięki temu można łatwo przystosować układ do nowych warunków pracy, bez konieczności przeszukiwania kodu i zmieniania wartości bezpośrednio w nim, co może prowadzić do powstawania bugów i ogólnej niestabilności systemu.

Przykład: trzymanie wszystkich wyżej wymienionych stałych w specjalnym pliku, na przykład `Config.hpp`, jako wartości `constexpr`
<details>
<summary>Pokaż przykład</summary>

```cpp
#ifndef CONFIG_HPP
#define CONFIG_HPP
#include <inttypes.h>

namespace Config {
  constexpr uint8_t ButtonXPin = 10;
  constexpr uint8_t ButtonYPin = 12;

  constexpr uint32_t BaudRate = 250000;
  constexpr char CommandTerminator = '\n';
}

#endif
```
</details>


Tą zasadę można stosować również dla literałów tekstowych używanych przy komunikacji, z takich samych powodów. Sugeruję jednak trzymać takie literały w innym pliku.
<details>
<summary>Pokaż przykład</summary>

```cpp
#ifndef COMMSTRINGS_HPP
#define COMMSTRINGS_HPP

namespace CommandKey {
  constexpr char const* Enable = "en";
  constexpr char const* Disable = "dis";
  constexpr char const* Value = "val";
}

#endif
```
</details>

#### 3. Dokumentacja

##### 3.1 Dokumentacja układu

Układ w którym powinien docelowo pracować mikrokontroler powinien posiadać dokumentację zawierającą **graficzny schemat połączeń**. Do stworzenia takiego schematu można zastosować dowolne narzędzie pozwalające modelować układy elektroniczne, na przykład [Fritzing](http://fritzing.org/home/) lub [EasyEDA](https://easyeda.com)

Schemat w postaci graficznej (oraz pliku z projektem do modyfikacji, jeśli to możliwe) powinien być dołączony do projektu na Githubie/Gitlabie.

##### 3.2 Dokumentacja kodu

Kod powinien posiadać komentarze, możliwie w stylu [Doxygena (preferowany jest styl Qt)](http://www.doxygen.nl/docblocks.html), opisujące klasy oraz funkcje/metody, oraz opcjonalne komentarze dotyczące działania kodu, jeśli będzie to konieczne.

Przykłady użycia danych klas i funkcji powinny znajdować się w testach.

##### 3.3 Dokumentacja komunikacji

Protokół komunikacyjny powinien być odpowiednio opisany w celu łatwej integracji z resztą systemu.

Dobrym opisem jest na przykład podanie ogólnego formatu danych, oraz poszczególnych poleceń razem z dokładnymi opisami i przykładami użycia.

Komunikacja powinna posiadać w miarę możliwości jeden ustalony bazowy format wiadomości, co ułatwi zarówno tworzenie ich jak i parsowanie.

Przykładem odpowiednio opisanego protokołu komunikacyjnego (JSON) jest [ta dokumentacja](https://github.com/SteelPh0enix/WirePuller/blob/master/README.md)