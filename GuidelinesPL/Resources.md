# Materiały do nauki

## Arduino

* [Oficjalna strona referencji Arduino](https://www.arduino.cc/reference/en/#page-title)
* [Oficjalna strona PlatformIO IDE](https://platformio.org/platformio-ide)
* [Oficjalna strona Visual Studio Code](https://code.visualstudio.com)
* [Dokumentacja PlatformIO](https://docs.platformio.org/en/latest/)
  * [Dokumentacja platformio.ini](https://docs.platformio.org/en/latest/projectconf.html)
* [Wsparcie kompilatorów oraz feature'y standardów języka C++](https://en.cppreference.com/w/cpp/compiler_support)
* [Fritzing - program do tworzenia graficznych schematów połączeń](http://fritzing.org/home/)
* [EasyEDA - program do tworzenia układów elektronicznych](https://easyeda.com)
* [Oficjalna strona Pythona](https://www.python.org)
* [Github](https://github.com) oraz [Gitlab](https://gitlab.com)
  * [Krótki kurs używania gita](https://git-scm.com/book/pl/v1/Pierwsze-kroki-Podstawy-Git)
